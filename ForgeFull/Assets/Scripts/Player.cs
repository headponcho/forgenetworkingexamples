﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BeardedManStudios.Forge.Networking.Generated;
using UnityStandardAssets.Characters.FirstPerson;
using BeardedManStudios.Forge.Networking;

public class Player : PlayerBehavior {

    private string[] nameParts = new string[] { "crazy", "cat", "dog", "homie", "bobble", "mr", "ms", "mrs", "castle", "flip", "flop" };

    public string name { get; private set; }

    protected override void NetworkStart() {
        base.NetworkStart();

        if (!networkObject.IsOwner) {
            transform.GetChild(0).gameObject.SetActive(false);
            GetComponent<FirstPersonController>().enabled = false;
            Destroy(GetComponent<Rigidbody>());
        }

        ChangeName();
    }

    public void ChangeName() {
        if (!networkObject.IsOwner) { return; }

        int first = Random.Range(0, nameParts.Length - 1);
        int last = Random.Range(0, nameParts.Length - 1);

        name = nameParts[first] + " " + nameParts[last];

        networkObject.SendRpc(RPC_UPDATE_NAME, Receivers.AllBuffered, name);
    }
	
	// Update is called once per frame
	void Update () {
		if (!networkObject.IsOwner) {
            transform.position = networkObject.position;
            return;
        }

        networkObject.position = transform.position;
	}

    public override void UpdateName(RpcArgs args) {
        name = args.GetNext<string>();
    }
}
