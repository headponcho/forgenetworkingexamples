﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BeardedManStudios.Forge.Networking.Generated;
using BeardedManStudios.Forge.Networking.Unity;
using BeardedManStudios.Forge.Networking;

public class GameLogic : GameLogicBehavior {

    public Text scoreLabel;

	// Use this for initialization
	void Start () {
        NetworkManager.Instance.InstantiatePlayer(position: new Vector3(0, 5, 0));
    }

    public override void PlayerScored(RpcArgs args) {
        string playerName = args.GetNext<string>();
        scoreLabel.text = "Last player to score was: " + playerName;
    }
}
