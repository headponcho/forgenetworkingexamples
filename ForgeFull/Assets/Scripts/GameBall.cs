﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BeardedManStudios.Forge.Networking.Generated;
using BeardedManStudios.Forge.Networking.Unity;

public class GameBall : GameBallBehavior {

    private Rigidbody rigidbodyRef;
    private GameLogic gameLogic;

    private void Awake() {
        rigidbodyRef = GetComponent<Rigidbody>();
        gameLogic = FindObjectOfType<GameLogic>();
    }
	
	// Update is called once per frame
	void Update () {
		if (!networkObject.IsOwner) {
            transform.position = networkObject.position;
            return;
        }

        networkObject.position = transform.position;
	}

    public void Reset() {
        transform.position = Vector3.up * 10;

        rigidbodyRef.velocity = Vector3.zero;

        Vector3 force = new Vector3(0, 0, 0);
        force.x = Random.Range(300, 500);
        force.z = Random.Range(300, 500);

        if (Random.value < 0.5f) {
            force.x *= -1;
        }
        if (Random.value < 0.5f) {
            force.z *= -1;
        }

        rigidbodyRef.AddForce(force);
    }

    private void OnCollisionEnter(Collision collision) {
        if (!networkObject.IsServer) { return; }

        Player player = collision.gameObject.GetComponent<Player>();

        if (player == null) { return; }

        gameLogic.networkObject.SendRpc(GameLogicBehavior.RPC_PLAYER_SCORED, BeardedManStudios.Forge.Networking.Receivers.All, player.name);

        Reset();
    }
}
