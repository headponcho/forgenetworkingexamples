﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BeardedManStudios.Forge.Networking.Unity;

public class GameTrigger : MonoBehaviour {

    private bool started;

	// Update is called once per frame
	void Update () {
		if (FindObjectOfType<GameBall>() != null) {
            Destroy(gameObject);
        }
	}

    private void OnTriggerEnter(Collider other) {
        if (started) { return; }

        if (!NetworkManager.Instance.IsServer) { return; }

        Player player = other.gameObject.GetComponent<Player>();

        if (player == null) { return; }

        started = true;

        GameBall ball = NetworkManager.Instance.InstantiateGameBall() as GameBall;

        ball.Reset();

        Destroy(gameObject);
    }
}
