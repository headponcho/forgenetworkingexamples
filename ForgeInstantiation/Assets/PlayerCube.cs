﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BeardedManStudios.Forge.Networking.Generated;

public class PlayerCube : PlayerCubeBehavior {

    public float speed = 5.0f;
	
	// Update is called once per frame
	void Update () {
		if (!networkObject.IsOwner) {
            transform.position = networkObject.position;
            transform.rotation = networkObject.rotation;
            return;
        }

        Vector3 translation = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")).normalized;
        translation *= speed * Time.deltaTime;

        transform.position += translation;
        transform.Rotate(new Vector3(speed, speed, speed) * 0.25f);

        networkObject.position = transform.position;
        networkObject.rotation = transform.rotation;
	}
}
